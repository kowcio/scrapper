package  scrapper.service.twitter;

import java.io.IOException;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ScrapperTest {
    static Logger log = LoggerFactory.getLogger(ScrapperTest.class);

    public static void scrape() throws IOException {
       
        Document doc = Jsoup.connect("http://hyper.directory/ocala").get();
        
        Elements mainLinks = doc.getElementsByAttributeValueMatching
           ("href",Pattern.compile("http://hyper.directory/ocala/.*?"));
        
        log.info("Links : {}",mainLinks);
        
        
        
    }

}
