package allinone.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.activemq.filter.function.makeListFunction;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.neo4j.cypher.internal.compiler.v2_1.perty.docbuilders.docStructureDocBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import scrapper.Application;
import scrapper.service.twitter.ScrapperTest;

import com.fasterxml.jackson.core.JsonProcessingException;

public class ServiceTest {

    private static final int    TWITTER_TEST_TIMEOUT = 10000;
    private static Logger       log                  = LoggerFactory.getLogger(ServiceTest.class);
    private static final String BASE                 = "http://hyper.directory/";

    @Test
    public void ScraperTest() throws IOException {

        Document doc = Jsoup.connect(BASE + "ocala").get();
        log.info(doc.title());
        Pattern pattern = Pattern.compile("ocala/.*?", Pattern.DOTALL);
        Elements mainLinks = doc.getElementsByAttributeValueMatching("href", pattern);

        
        
        for(Element e : mainLinks){
            String urlSubPage = e.absUrl("href");
            Elements pageElemenets = getPages(urlSubPage);
            System.out.println("URL  = " + urlSubPage);
//            System.out.println(pageElemenets);
            for(Element ie : pageElemenets){
                List<String> str = getListingInnerBoxes(ie);
//              System.out.println(str);
              //get link for certain website
              
              for(String subPage:str){
              String getFinals=getFinals(subPage);
              System.out.println(getFinals);
              }
//break;
            }
//            break;
        }

    }
public static String getFinals(String url) throws IOException{
    String name = null;
    String add = null;
    String web = null;
    try {
        Document doc = Jsoup.connect("http://hyper.directory" + url.replace("..","")).get();
        name = doc.getElementsByAttributeValueMatching("id", "MainContent_lblCompanyName").text();
        add = doc.getElementsByAttributeValueMatching("id", "MainContent_divAddress").text();
        web = doc.getElementsByAttributeValueMatching("id", "MainContent_hlWeb").text();
    } catch (Exception e) {
//        e.printStackTrace();
    }
    

    return name+";"+add+";"+web;
    
}
// business name, address, and website url.

    public static Elements getPages(String subPage) {
        Document doc = null;
        try {
            doc = Jsoup.connect(subPage).get();
        } catch (IOException e) {
        }
        Pattern pattern = Pattern.compile("ocala/.*?", Pattern.DOTALL);
        Elements mainLinks = doc.getElementsByAttributeValueMatching("class", "ListingBoxInner");
//        log.info("{}",mainLinks);

        return mainLinks;
    }

    // business name, address, and website url.
    public static List<String> getListingInnerBoxes(Element e) {

        String bn = null;
        String ph = null;
        String ad = null;
        try {
            return e.getElementsByAttributeValueMatching("href", Pattern.compile(".*?ListingDetail.*?"))
                    .stream()
                    .map(x->x.attr("href"))
                    .collect(Collectors.toCollection(ArrayList::new));
//                    .attr("href");//.getElementsByAttribute("href").html();
//            ph = e.getElementsByAttributeValue("class", "ListingBusinessPhone").get(0).text();
//            ad = e.getElementsByAttributeValueMatching("class", Pattern.compile("ListingBusinessAddress")).get(0).text();
        } catch (Exception e1) {
//            e1.printStackTrace();
        }
        
Elements csv = null;
        //        String csv = bn;// + ";" + ph + ";" + ad;
        return new ArrayList<>();
    }

}
